import java.util.Objects;

public class Product {
    private final Integer Id;
    private final String Title;
    private double Cost;
    private Integer Quantity;

    public Product(Integer Id, String Name, double Cost, Integer Quantity) {
        this.Id = Id;
        this.Title = Name;
        this.Cost = Cost;
        this.Quantity = Quantity;
    }

    @Override
    public boolean equals(Object i) {
        if (this == i) return true;
        if (!(i instanceof Product product)) return false;
        return Objects.equals(Id, product.Id) &&
                Double.compare(product.Cost, Cost) == 0 && Objects.equals(Quantity, product.Quantity) &&
                Objects.equals(Title, product.Title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, Title, Cost, Quantity);
    }

    public Integer getId() {
        return Id;
    }

    public String getTitle() {
        return Title;
    }

    public double getCost() {
        return Cost;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setCost(double Cost) {
        this.Cost = Cost;
    }

    public void setQuantity(Integer Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "Id=" + Id +
                ", Name='" + Title + '\'' +
                ", cost=" + Cost +
                ", Quantity=" + Quantity +
                '}';
    }
}
