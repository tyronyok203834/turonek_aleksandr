import java.util.List;

public interface ProductsRepository {
    Product productFindById(Integer Id);

    List<Product> findAllByTitleLike(String title);

    void update(Product product);

}
