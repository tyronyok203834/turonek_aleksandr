package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.model.Book;
import ru.inno.ec.model.Section;
import java.util.List;

public interface BooksRepository extends JpaRepository<Book,Long> {
    List<Book>findAllByStatusNot(Book.Status state);

    List<Book> findAllBySectionNull();

    List<Book> findAllBySection(Section section);
}




