package ru.inno.ec.service;

import ru.inno.ec.dto.SectionForm;
import ru.inno.ec.model.Book;
import ru.inno.ec.model.Section;
import ru.inno.ec.model.User;

import java.util.List;

public interface SectionService {
    List<Section>getAllSections();
    void addReaderToSection(Long sectionId, Long readerId);

    Section getSection(Long sectionId);

    List<User> getNotInSectionReaders(Long sectionId);

    List<User> getInSectionReaders(Long sectionId);

    void deleteSection(Long courseId);

    void addSection(SectionForm section);

    void updateSection(Long sectionId, SectionForm section);
    void addBookToSection(Long sectionId, Long bookId);
    List<Book> getNotInSectionBooks();
    List<Book> getInSectionBooks(Long sectionId);
}






