package ru.inno.ec.service;

import ru.inno.ec.dto.BookForm;
import ru.inno.ec.model.Book;

import java.util.List;

public interface BooksService {
    List<Book>getAllBooks();
    Book getBook(Long bookId);
    void deleteBook(Long bookId);
    void addBook(BookForm book);
    void updateBook(Long bookId,BookForm book);

}

