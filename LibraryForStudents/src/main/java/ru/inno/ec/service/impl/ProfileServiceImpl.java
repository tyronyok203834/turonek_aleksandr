package ru.inno.ec.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.model.User;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.service.ProfileService;

@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService {

    private final UsersRepository usersRepository;

    @Override
    public User getCurrent(CustomUserDetails userDetails) {
        return usersRepository.findById(userDetails.getUser().getId()).orElseThrow();
    }
}




