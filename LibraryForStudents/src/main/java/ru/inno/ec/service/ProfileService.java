package ru.inno.ec.service;

import ru.inno.ec.model.User;
import ru.inno.ec.security.details.CustomUserDetails;

public interface ProfileService {
    User getCurrent(CustomUserDetails userDetails);
}




