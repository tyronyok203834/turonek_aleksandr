package ru.inno.ec.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.dto.SectionForm;
import ru.inno.ec.model.Book;
import ru.inno.ec.model.Section;
import ru.inno.ec.model.User;
import ru.inno.ec.repositories.BooksRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.repositories.SectionsRepository;
import ru.inno.ec.service.SectionService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class SectionsServiceImpl implements SectionService {

    private final SectionsRepository sectionsRepository;
    private final UsersRepository usersRepository;
    private final BooksRepository booksRepository;

    @Override
    public List<Section> getAllSections() {
        return sectionsRepository.findAllByStateNot(Section.State.DELETED);
    }

    @Override
    public void addReaderToSection(Long sectionId, Long readerId) {
        Section section = sectionsRepository.findById(sectionId).orElseThrow();
        User reader = usersRepository.findById(readerId).orElseThrow();
        reader.getSections().add(section);
        usersRepository.save(reader);
    }
    @Override
    public void addBookToSection(Long sectionId, Long bookId) {
        Section section = sectionsRepository.findById(sectionId).orElseThrow();
        Book book = booksRepository.findById(bookId).orElseThrow();
        book.setSection(section);
        booksRepository.save(book);

    }
    @Override
    public Section getSection (Long sectionId) {
        return sectionsRepository.findById(sectionId).orElseThrow();
    }

    @Override
    public List<User> getNotInSectionReaders(Long sectionId) {
        Section section = sectionsRepository.findById(sectionId).orElseThrow();
        return usersRepository.findAllBySectionsNotContains(section);
    }

    @Override
    public List<User> getInSectionReaders(Long sectionId) {
        Section section = sectionsRepository.findById(sectionId).orElseThrow();
        return usersRepository.findAllBySectionsContains(section);
    }

    @Override
    public void deleteSection(Long sectionId) {
        Section sectionForDelete = sectionsRepository.findById(sectionId).orElseThrow();
        sectionForDelete.setState(Section.State.DELETED);

        sectionsRepository.save(sectionForDelete);
    }

    @Override
    public void addSection(SectionForm section) {
        Section newSection = Section.builder()
                .name(section.getName())
                .description(section.getDescription())
                .state(Section.State.NOT_CONFIRMED)
                .build();
        sectionsRepository.save(newSection);
    }

    @Override
    public void updateSection(Long sectionId, SectionForm updateDate) {
        Section sectionForUpdate = sectionsRepository.findById(sectionId).orElseThrow();
        sectionForUpdate.setName(updateDate.getName());
        sectionForUpdate.setDescription(updateDate.getDescription());
        sectionsRepository.save(sectionForUpdate);
    }


    @Override
    public List<Book> getNotInSectionBooks() {
        return booksRepository.findAllBySectionNull();
    }

    @Override
    public List<Book> getInSectionBooks(Long sectionId) {
        Section section = sectionsRepository.findById(sectionId).orElseThrow();
        return booksRepository.findAllBySection(section);
    }
}






